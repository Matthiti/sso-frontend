# Single sign-on frontend

This is the frontend of the roelink.eu single sign-on. The backend can be found [here](https://gitlab.com/Matthiti/single-sign-on).

## Requirements

- `node 16`

## Project setup
```
npm install
```

## Run
```
npm run serve
```

## Build
```
npm run build
```
