import config from '@/config';

function getFullImageUrl(imageUrl) {
  return imageUrl.startsWith("http://") || imageUrl.startsWith("https://")
    ? imageUrl
    : config.baseUrl + imageUrl;
}

function isValidPassword(password) {
  return password.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,}$/);
}

export default {
  getFullImageUrl,
  isValidPassword
}
