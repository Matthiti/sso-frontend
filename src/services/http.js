import axios from "axios";
import config from "../config";
import Store from "../store";
import Router from "../router";

axios.defaults.baseURL = config.apiUrl;

let interceptor;

if (isLoggedIn()) {
  const token = getTokenFromCookie();
  axios.defaults.headers.common["X-Authorization"] = `Bearer ${token}`;
  enableInterceptor();
}

function enableInterceptor() {
  interceptor = axios.interceptors.response.use(null, function(error) {
    if (error.response && (error.response.status === 401 || error.response.status === 403)) {
      if (hasRefreshToken()) {
        refresh();
      } else {
        logout();
        Router.push("/");
      }
    } else {
      return Promise.reject(error);
    }
  });
}


function isLoggedIn() {
  return getTokenFromCookie() !== null && getTokenFromCookie !== '';
}

function getTokenFromCookie() {
  const cookieToken = document.cookie.split(';').map(c => c.trim()).find(c => c.startsWith('token='));
  return cookieToken ? cookieToken.substring('token='.length) : null;
}

function hasRefreshToken() {
  return localStorage.getItem("refreshToken") !== null;
}

function refresh() {
  axios.post('/auth/refresh', {refreshToken: localStorage.getItem('refreshToken')}).then(response => {
    afterLogin(response.data.token);
    location.reload();
  }).catch(() => {
    logout();
    Router.push("/");
  });
}

function afterLogin(token) {
  document.cookie = `token=${token};domain=${config.cookieDomain};path=/;Secure`;
  axios.defaults.headers.common["X-Authorization"] = "Bearer " + token;
  enableInterceptor();
}

function logout() {
  document.cookie = `token=;domain=${config.cookieDomain};path=/;Secure`;
  localStorage.removeItem("refreshToken");
  Store.commit("setUser", null);
  Store.commit("setPreferences", null);
  axios.interceptors.response.eject(interceptor);
}

export default {
  isLoggedIn,
  getTokenFromCookie,
  afterLogin,
  logout,
  axios
};
