import Vue from 'vue'
import Vuex from 'vuex'
import http from "./services/http";
import utils from '@/utils';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: null,
    preferences: null,
    domains: [],
    roles: [],
    users: [],
    locales: [
      {
        flag: 'gb',
        locale: 'en'
      },
      {
        flag: 'nl',
        locale: 'nl'
      }
    ],
    notifications: []
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    },
    setDomains(state, domains) {
      state.domains = domains;
    },
    setRoles(state, roles) {
      state.roles = roles;
    },
    setUsers(state, users) {
      state.users = users;
    },
    setPreferences(state, preferences) {
      state.preferences = preferences;
    },
    setNotifications(state, notifications) {
      state.notifications = notifications;
    }
  },
  actions: {
    fetchUser({commit}) {
      return new Promise(resolve => {
        http.axios.get('/me').then(response => {
          commit('setUser', response.data);
          resolve();
        });
      });
    },
    fetchPreferences({state, commit}) {
      return new Promise(resolve => {
        http.axios.get(`/users/${state.user.uuid}/preferences`).then(response => {
          commit('setPreferences', response.data);
          resolve();
        });
      });
    },
    fetchDomains({commit}) {
      return new Promise(resolve => {
        http.axios.get('/domains').then(response => {
          commit('setDomains', response.data);
          resolve();
        })
      });
    },
    fetchRoles({commit}) {
      return new Promise(resolve => {
        http.axios.get('/roles').then(response => {
          commit('setRoles', response.data);
          resolve();
        });
      });
    },
    fetchUsers({commit}) {
      return new Promise(resolve => {
        http.axios.get('/users').then(response => {
          commit('setUsers', response.data);
          resolve();
        });
      });
    },
    fetchNotifications({commit}) {
      return new Promise(resolve => {
        http.axios.get('/notifications').then(response => {
          commit('setNotifications', response.data);
          resolve();
        });
      });
    }
  },
  getters: {
    user: state => state.user,
    domains: state => state.domains,
    roles: state => state.roles,
    users: state => state.users,
    preferences: state => state.preferences,
    locales: state => state.locales,
    notifications: state => state.notifications,
    profilePicture(state) {
      if (!state.user || !state.user.imageURL) {
        return null
      }
      return utils.getFullImageUrl(state.user.imageURL);
    },
    initials(state) {
      if (!state.user) {
        return null
      }
      return state.user.firstname.charAt(0).toUpperCase() + state.user.lastname.charAt(0).toUpperCase();
    },
    localesWithout: (state) => (locale) => {
      return state.locales.filter(l => l.locale !== locale);
    },
    flagOfLocale: (state) => (locale) => {
      return state.locales.find(l => l.locale === locale).flag;
    }
  }
})
