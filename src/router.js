import Vue from 'vue'
import Router from 'vue-router'
import Account from "./pages/Account";
import Default from "./components/Default";
import NotFound from "./pages/NotFound";
import Dashboard from "./pages/Dashboard";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Admin from "./pages/Admin";
import Logout from "./pages/Logout";
import ForgotPassword from "./pages/ForgotPassword";
import ResetPassword from "./pages/ResetPassword";
import NoAuth from "./components/NoAuth";

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '',
      component: Default,
      children: [
        {
          path: '/account',
          name: 'account',
          component: Account
        },
        {
          path: '/dashboard',
          name: 'dashboard',
          component: Dashboard
        },
        {
          path: '/admin',
          name: 'admin',
          component: Admin
        },
        {
          path: '/',
          redirect: {name: 'login'}
        }
      ]
    },
    {
      path: '',
      component: NoAuth,
      children: [
        {
          path: '/',
          name: 'login',
          component: Login
        },
        {
          path: '/forgot-password',
          name: 'forgotPassword',
          component: ForgotPassword
        },
        {
          path: '/reset-password',
          name: 'resetPassword',
          component: ResetPassword
        },
        {
          path: '/register',
          name: 'register',
          component: Register
        },
        {
          path: '/logout',
          name: 'logout',
          component: Logout
        },
        {
          path: '*',
          component: NotFound
        }
      ]
    },
  ]
})
