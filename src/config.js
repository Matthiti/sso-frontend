let baseUrl = "https://sso.roelink.eu";
let cookieDomain = "roelink.eu";
if (window.location.href.indexOf("localhost") !== -1) {
  baseUrl = "http://localhost:8000";
  cookieDomain = "localhost";
}

const apiUrl = baseUrl + "/api/v1";

export default {
  baseUrl,
  apiUrl,
  cookieDomain
}
