let ssoURL = "https://login.roelink.eu";
if (window.location.href.indexOf("localhost") !== -1) {
  ssoURL = "http://localhost:8080";
}

window.onload = function() {
  const cookieToken = document.cookie.split(';').map(c => c.trim()).find(c => c.startsWith('token='));
  if (!cookieToken) {
    window.location.href = `${ssoURL}?redirect=${window.location.href}`;
    return;
  }

  const token = cookieToken.substring('token='.length);
  if (!isValidToken(token)) {
    window.location.href = `${ssoURL}?redirect=${window.location.href}`;
  } else {
    createEvent(token);
  }
}

function createEvent(token) {
  let event = new CustomEvent('tokenLoaded', { detail: token });
  window.dispatchEvent(event);
}

function decodeToken(token) {
  let base64Url = token.split('.')[1];
  let base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  let jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));

  return JSON.parse(jsonPayload);
}

function isValidToken(token) {
  if (!token) {
    return false;
  }

  let exp = decodeToken(token).exp;
  return new Date().getTime() < exp * 1000;
}